#include "mcc_generated_files/system/system.h"
#include "mcc_generated_files/timer/delay.h"

#include "tcs3471/tcs3471.h"


void delay_ms(uint32_t ms){
    DELAY_milliseconds(ms);
}

uint8_t i2c_write(uint16_t client_address, uint8_t *data, uint8_t size){
    TWI0_Write(client_address, data, size);
    while(TWI0_IsBusy());
    return !(TWI0_ErrorGet());
}

uint8_t i2c_read(uint16_t client_address, uint8_t *data, uint8_t size){
    TWI0_Read(client_address, data, size);
    while(TWI0_IsBusy());
    return !(TWI0_ErrorGet());
}


int main(void)
{
    SYSTEM_Initialize();
    
    tcs3471_t tcs3471 = {};
    
    tcs3471_i2c_read_register(&tcs3471, i2c_read);
    tcs3471_i2c_write_register(&tcs3471, i2c_write);
    
    tcs3471_config_t config;
    config.aien = TCS3471_ENABLE_AIEN_RGBC_INT_DISABLE;
    config.wen  = TCS3471_ENABLE_WEN_WAIT_DISABLE;
    config.aen  = TCS3471_ENABLE_AEN_RGBC_ENABLE;
    config.pon  = TCS3471_ENABLE_PON_POWER_ON;
    
    config.atime = 0;
    config.wtime = 0;
    
    config.aiht = 90 * (100 / 65535.0);
    config.ailt = 10 * (100 / 65535.0);
    
    config.apers = TCS3471_PERSISTENCE_APERS_EVERY;
    
    config.again = TCS3471_CONTROL_AGAIN_1x_GAIN;

    if(tcs3471_init(&tcs3471, TCS3471_I2C_CLIENT_ADDRESS, &config)){
        printf("INIT\r\n");
    }
    else{
        printf("I2C ERROR\r\n");
    }

    while(1)
    {
        
        if(!tcs3471_get_data(&tcs3471) && !tcs3471_get_device_id(&tcs3471)){
            printf("I2C ERROR\r\n");
        }
        else{
            printf("device id = 0x%02X\r\n", tcs3471.data.device_id);
            printf("clear = %.02f %%, red = %.02f %%, green = %.02f %%, blue = %.02f %%\r\n", (tcs3471.data.color.clear / 65535.0) * 100.0, (tcs3471.data.color.red / 65535.0) * 100.0, (tcs3471.data.color.green / 65535.0) * 100.0, (tcs3471.data.color.blue / 65535.0) * 100.0);
        }
        
        delay_ms(500);
        
    }    
}
