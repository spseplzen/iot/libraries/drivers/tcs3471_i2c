/*
 * File:   tcs3471.c
 * Author: Miroslav Soukup
 * Description: Source file of tcs3471 color sensor driver.
 * 
 */



// *****************************************************************************
// Section: Included Files
// *****************************************************************************

#include <stdint.h> // data types
#include <stdlib.h>

#include "tcs3471.h"


// *****************************************************************************
// Section: Functions
// *****************************************************************************

static uint8_t _tcs3471_i2c_read_reg(tcs3471_t *me, uint8_t reg, uint8_t *byte){
    uint8_t _reg = 1 << 7 | reg;
    if(!me->i2c_write(me->client_address, &_reg, 1)) return 0;
    if(!me->i2c_read(me->client_address, byte, 1)) return 0;
    return 1;
}

static uint8_t _tcs3471_i2c_write_reg(tcs3471_t *me, uint8_t reg, uint8_t byte){
    uint8_t _data[2];
    _data[0] = 1 << 7 | reg;
    _data[1] = byte;
    if(!me->i2c_write(me->client_address, _data, 2)) return 0;
    return 1;
}

uint8_t tcs3471_init (tcs3471_t *me, uint16_t client_address, tcs3471_config_t *config) {
    
    me->config = *config;
    
    me->client_address = client_address;
    
    /* ************************* ENABLE ************************** */
    if(!_tcs3471_i2c_write_reg(me, TCS3471_REG_ENABLE, me->config.aien << 4 | me->config.wen << 3 | me->config.aen << 1 | me->config.pon)) return 0;
    /* *********************************************************** */
    
    /* ************************** ATIME ************************** */
    if(!_tcs3471_i2c_write_reg(me, TCS3471_REG_ATIME, me->config.atime)) return 0;
    /* *********************************************************** */
    
    /* ************************** WTIME ************************** */
    if(!_tcs3471_i2c_write_reg(me, TCS3471_REG_WTIME, me->config.wtime)) return 0;
    /* *********************************************************** */
    
    /* ************************** AILTL ************************** */
    if(!_tcs3471_i2c_write_reg(me, TCS3471_REG_AILTL, me->config.ailt & 0x0F)) return 0;
    /* *********************************************************** */
    
    /* ************************** AILTH ************************** */
    if(!_tcs3471_i2c_write_reg(me, TCS3471_REG_AILTH, (me->config.ailt >> 8) & 0x0F)) return 0;
    /* *********************************************************** */
    
    /* ************************** AIHTL ************************** */
    if(!_tcs3471_i2c_write_reg(me, TCS3471_REG_AIHTL, me->config.aiht & 0x0F)) return 0;
    /* *********************************************************** */
    
    /* ************************** AIHTH ************************** */
    if(!_tcs3471_i2c_write_reg(me, TCS3471_REG_AIHTH, (me->config.ailt >> 8) & 0x0F)) return 0;
    /* *********************************************************** */
    
    /* ************************** PERS *************************** */
    if(!_tcs3471_i2c_write_reg(me, TCS3471_REG_PERS, me->config.apers)) return 0;
    /* *********************************************************** */
    
    /* ********************** Configuration ********************** */
    if(!_tcs3471_i2c_write_reg(me, TCS3471_REG_CONFIG, me->config.wlong << 1)) return 0;
    /* *********************************************************** */
    
    /* ************************* Control ************************* */
    if(!_tcs3471_i2c_write_reg(me, TCS3471_REG_CONTROL, me->config.again)) return 0;
    /* *********************************************************** */
    
    return 1;
}

uint8_t tcs3471_i2c_write_register (tcs3471_t *me, tcs3471_i2c_rw_funcptr_t i2c_write_funcptr){
    if(!i2c_write_funcptr) return 0;
    me->i2c_write = i2c_write_funcptr;
    return 1;
}

uint8_t tcs3471_i2c_read_register (tcs3471_t *me, tcs3471_i2c_rw_funcptr_t i2c_read_funcptr){
    if(!i2c_read_funcptr) return 0;
    me->i2c_read = i2c_read_funcptr;
    return 1;
}

uint8_t tcs3471_get_data (tcs3471_t *me){
    uint8_t _dl;
    uint8_t _dh;
    
    me->data.color.clear = 0;
    if(!_tcs3471_i2c_read_reg(me, TCS3471_REG_CDATA, &_dl)) return 0;
    if(!_tcs3471_i2c_read_reg(me, TCS3471_REG_CDATAH, &_dh)) return 0;
    me->data.color.clear = _dh << 8 | _dl;
    
    me->data.color.red = 0;
    if(!_tcs3471_i2c_read_reg(me, TCS3471_REG_RDATA, &_dl)) return 0;
    if(!_tcs3471_i2c_read_reg(me, TCS3471_REG_RDATAH, &_dh)) return 0;
    me->data.color.red = _dh << 8 | _dl;
    
    me->data.color.green = 0;
    if(!_tcs3471_i2c_read_reg(me, TCS3471_REG_GDATA, &_dl)) return 0;
    if(!_tcs3471_i2c_read_reg(me, TCS3471_REG_GDATAH, &_dh)) return 0;
    me->data.color.green = _dh << 8 | _dl;
    
    me->data.color.blue = 0;
    if(!_tcs3471_i2c_read_reg(me, TCS3471_REG_BDATA, &_dl)) return 0;
    if(!_tcs3471_i2c_read_reg(me, TCS3471_REG_BDATAH, &_dh)) return 0;
    me->data.color.blue = _dh << 8 | _dl;
    
    return 1;
}

uint8_t tcs3471_get_device_id (tcs3471_t *me){
    uint8_t dev_id;
    if(!_tcs3471_i2c_read_reg(me, TCS3471_REG_ID, &dev_id)) return 0;
    me->data.device_id = dev_id;
    return 1;
}

uint8_t tcs3471_i2c_check (tcs3471_t *me){
    uint8_t _byte;
    return me->i2c_read(me->client_address, &_byte, 1);
}

uint8_t tcs3471_get_device_status (tcs3471_t *me){
    uint8_t _byte;
    if(!_tcs3471_i2c_read_reg(me, TCS3471_REG_STATUS, &_byte)) return 0;
    me->status.aint = (_byte >> 4) & 1;
    me->status.avalid = _byte & 1;
    return 1;
}
