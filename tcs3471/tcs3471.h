/*
 * File:   tcs3471.h
 * Author: Miroslav Soukup
 * Description: Header file of tcs3471 color sensor driver.
 * 
 */



#ifndef TCS3471_H // Protection against multiple inclusion
#define TCS3471_H



// *****************************************************************************
// Section: Included Files
// *****************************************************************************

#include <stdint.h> // data types 



#ifdef __cplusplus  // Provide C++ compatibility
extern "C" {
#endif



// *****************************************************************************
// Section: Macros
// *****************************************************************************

#define TCS3471_I2C_CLIENT_ADDRESS                  UINT8_C(0x29)
    
#define TCS3471_REG_ENABLE                          UINT8_C(0x00)
#define TCS3471_REG_ATIME                           UINT8_C(0x01)
#define TCS3471_REG_WTIME                           UINT8_C(0x03)
#define TCS3471_REG_AILTL                           UINT8_C(0x04)
#define TCS3471_REG_AILTH                           UINT8_C(0x05)
#define TCS3471_REG_AIHTL                           UINT8_C(0x06)
#define TCS3471_REG_AIHTH                           UINT8_C(0x07)
#define TCS3471_REG_PERS                            UINT8_C(0x0C)
#define TCS3471_REG_CONFIG                          UINT8_C(0x0D)
#define TCS3471_REG_CONTROL                         UINT8_C(0x0F)
#define TCS3471_REG_ID                              UINT8_C(0x12)
#define TCS3471_REG_STATUS                          UINT8_C(0x13)
#define TCS3471_REG_CDATA                           UINT8_C(0x14)
#define TCS3471_REG_CDATAH                          UINT8_C(0x15)
#define TCS3471_REG_RDATA                           UINT8_C(0x16)
#define TCS3471_REG_RDATAH                          UINT8_C(0x17)
#define TCS3471_REG_GDATA                           UINT8_C(0x18)
#define TCS3471_REG_GDATAH                          UINT8_C(0x19)
#define TCS3471_REG_BDATA                           UINT8_C(0x1A)
#define TCS3471_REG_BDATAH                          UINT8_C(0x1B)



// *****************************************************************************
// Section: Data types
// *****************************************************************************

typedef struct tcs3471_descriptor tcs3471_t;
typedef struct tcs3471_data_descriptor tcs3471_data_t;
typedef struct tcs3471_data_color_descriptor tcs3471_data_color_t;
typedef struct tcs3471_config_descriptor tcs3471_config_t;
typedef struct tcs3471_status_descriptor tcs3471_status_t;

typedef uint8_t (*tcs3471_i2c_rw_funcptr_t)(uint16_t client_address, uint8_t *data, uint8_t size);


typedef enum{
    TCS3471_ENABLE_AIEN_RGBC_INT_DISABLE = 0b0, // power-up default
    TCS3471_ENABLE_AIEN_RGBC_INT_ENABLE  = 0b1
} TCS3471_ENABLE_AIEN_e;

typedef enum{
    TCS3471_ENABLE_WEN_WAIT_DISABLE = 0b0, // power-up default
    TCS3471_ENABLE_WEN_WAIT_ENABLE  = 0b1
} TCS3471_ENABLE_WEN_e;

typedef enum{
    TCS3471_ENABLE_AEN_RGBC_DISABLE = 0b0, // power-up default
    TCS3471_ENABLE_AEN_RGBC_ENABLE  = 0b1
} TCS3471_ENABLE_AEN_e;

typedef enum{
    TCS3471_ENABLE_PON_POWER_OFF = 0b0, // power-up default
    TCS3471_ENABLE_PON_POWER_ON  = 0b1
} TCS3471_ENABLE_PON_e;

typedef enum{
    TCS3471_PERSISTENCE_APERS_EVERY = 0b0000, // power-up default
    TCS3471_PERSISTENCE_APERS_1     = 0b0001,
    TCS3471_PERSISTENCE_APERS_2     = 0b0010,
    TCS3471_PERSISTENCE_APERS_3     = 0b0011,
    TCS3471_PERSISTENCE_APERS_5     = 0b0100,
    TCS3471_PERSISTENCE_APERS_10    = 0b0101,
    TCS3471_PERSISTENCE_APERS_15    = 0b0110,
    TCS3471_PERSISTENCE_APERS_20    = 0b0111,
    TCS3471_PERSISTENCE_APERS_25    = 0b1000,
    TCS3471_PERSISTENCE_APERS_30    = 0b1001,
    TCS3471_PERSISTENCE_APERS_35    = 0b1010,
    TCS3471_PERSISTENCE_APERS_40    = 0b1011,
    TCS3471_PERSISTENCE_APERS_45    = 0b1100,
    TCS3471_PERSISTENCE_APERS_50    = 0b1101,
    TCS3471_PERSISTENCE_APERS_55    = 0b1110,
    TCS3471_PERSISTENCE_APERS_60    = 0b1111
} TCS3471_PERSISTENCE_APERS_e;

typedef enum{
    TCS3471_CONFIGURATION_WLONG_WAIT_LONG_DISABLE = 0b0, // power-up default
    TCS3471_CONFIGURATION_WLONG_WAIT_LONG_ENABLE  = 0b1
} TCS3471_CONFIGURATION_WLONG_e;

typedef enum{
    TCS3471_CONTROL_AGAIN_1x_GAIN  = 0b00, // power-up default
    TCS3471_CONTROL_AGAIN_4x_GAIN  = 0b01,
    TCS3471_CONTROL_AGAIN_16x_GAIN = 0b10,
    TCS3471_CONTROL_AGAIN_60x_GAIN = 0b11
} TCS3471_CONTROL_AGAIN_e;



struct tcs3471_status_descriptor{
    uint8_t aint;
    uint8_t avalid;
};

struct tcs3471_config_descriptor{
    TCS3471_ENABLE_AIEN_e aien; // RGBC INT enable
    TCS3471_ENABLE_WEN_e wen;  // Wait enable
    TCS3471_ENABLE_AEN_e aen;  // RGBC enable
    TCS3471_ENABLE_PON_e pon;  // Power ON
    
    uint8_t atime; // RGBC timming
    
    uint8_t wtime; // RGBC longer timming
    
    uint16_t ailt; // RGBC low treshold
    
    uint16_t aiht; // RGBC high treshold

    TCS3471_PERSISTENCE_APERS_e apers; // persistence
    
    TCS3471_CONFIGURATION_WLONG_e wlong; // Wait long enable
    
    TCS3471_CONTROL_AGAIN_e again; // RGBC Gain Control
};

struct tcs3471_data_color_descriptor{
    uint16_t clear;
    uint16_t red;
    uint16_t green;
    uint16_t blue;
};

struct tcs3471_data_descriptor{
    uint8_t device_id;
    tcs3471_data_color_t color;
};

struct tcs3471_descriptor{
    uint16_t client_address;
    
    tcs3471_config_t config;
    
    tcs3471_status_t status;
    
    tcs3471_i2c_rw_funcptr_t i2c_write;
    tcs3471_i2c_rw_funcptr_t i2c_read;
    
    tcs3471_data_t data;
};



// *****************************************************************************
// Section: Function prototypes
// *****************************************************************************

/**
 * \brief Function for initialization tcs3471 module.
 *
 * \param me pointer to tcs3471 sensor type of tcs3471_t
 * \param config pointer to tcs3471 configuration type of tcs3471_config_t
 *
 * \returns status of success / failure
 */
uint8_t tcs3471_init (tcs3471_t *me, uint16_t client_address, tcs3471_config_t *config);

uint8_t tcs3471_i2c_write_register (tcs3471_t *me, tcs3471_i2c_rw_funcptr_t i2c_write_funcptr);

uint8_t tcs3471_i2c_read_register (tcs3471_t *me, tcs3471_i2c_rw_funcptr_t spi_read_funcptr);

uint8_t tcs3471_get_data (tcs3471_t *me);

uint8_t tcs3471_get_device_id (tcs3471_t *me);

uint8_t tcs3471_i2c_check (tcs3471_t *me);

uint8_t tcs3471_get_device_status (tcs3471_t *me);



#ifdef __cplusplus // End of C++ compatibility
}
#endif

    
#endif // End of TCS3471_H
